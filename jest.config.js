module.exports = {
  preset: './src/test/preset.js',
  testEnvironment: 'node',
  testURL: 'http://dummy.com:3000',
  setupFilesAfterEnv: ['./src/test/setup.ts'],
  testPathIgnorePatterns: ['/node_modules/', '/dist/'],
};
