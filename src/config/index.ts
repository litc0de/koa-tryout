import dotenv from 'dotenv';

// overrides
if (process.env.NODE_ENV === 'test') dotenv.config({ path: '.env.test' });

dotenv.config({ path: '.env' });
dotenv.config({ path: '.secrets.env' });
