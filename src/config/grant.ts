export default {
  server: {
    protocol: 'http',
    host: 'dummy.com:8080',
    transport: 'session',
    path: '/api',
  },

  twitter: {
    key: process.env.TWITTER_API_KEY,
    secret: process.env.TWITTER_API_SECRET_KEY,
    callback: '/handle_twitter_callback',
  },
};
