import '../config';

import { resolve } from 'path';
import mongoose from 'mongoose';
import dirTree from 'directory-tree';

import { Conference, ConferenceImport, ConferenceModel } from '../Conferences';

const path = resolve('node_modules/the-conference-javascript-list/conferences');
const tree = dirTree(path, { extensions: /\.json$/ });

const mapConferences = async (
  typeFile: dirTree.DirectoryTree,
  year: string
): Promise<Conference[]> => {
  const type = typeFile.name.match(/.*(?=\.json)/)[0];
  const importedConferences: ConferenceImport[] = (await import(typeFile.path))
    .default;

  return importedConferences.map((conferenceImport: ConferenceImport) => {
    const conference: Conference = {
      ...conferenceImport,
      type,
      year,
      attendees: [],
    };
    return conference;
  });
};

const mapConfTypes = async (
  yearFolder: dirTree.DirectoryTree
): Promise<Conference[]> => {
  const year = yearFolder.name;
  const typeFiles = yearFolder.children;
  let conferences: Conference[] = [];

  for (const typeFile of typeFiles) {
    const newConferences = await mapConferences(typeFile, year);
    conferences = [...conferences, ...newConferences];
  }

  return conferences;
};

const getConferences = async (): Promise<Conference[]> => {
  const yearFolders = tree.children;
  let conferences: Conference[] = [];

  for (const yearFolder of yearFolders) {
    const newConferences = await mapConfTypes(yearFolder);
    conferences = [...conferences, ...newConferences];
  }

  return conferences;
};

const dbConnect = async (): Promise<mongoose.Mongoose> => {
  return await mongoose.connect(process.env.MONGO_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });
};

const writeConferences = async (): Promise<any> => {
  const connection = await dbConnect();
  const conferences = await getConferences();
  await ConferenceModel.insertMany(conferences);
  await connection.disconnect();
};

writeConferences();
