import OAuth from 'oauth';

const consumerKey = process.env.TWITTER_API_KEY;
const consumerSecret = process.env.TWITTER_API_SECRET_KEY;

const oauth = new OAuth.OAuth(
  'https://api.twitter.com/oauth/request_token',
  'https://api.twitter.com/oauth/access_token',
  consumerKey,
  consumerSecret,
  '1.0A',
  null,
  'HMAC-SHA1'
);

export default (
  url: string,
  accessToken: string,
  accessSecret: string
): Promise<any> =>
  new Promise((resolve, reject): void => {
    oauth.get(url, accessToken, accessSecret, (e, data) => {
      if (e) reject(e);
      resolve(data);
    });
  });
