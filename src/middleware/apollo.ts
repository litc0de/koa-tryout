import { ParameterizedContext } from 'koa';
import { ApolloServer } from 'apollo-server-koa';
import typeDefs from '../schema';
import resolvers from '../resolvers';
import dataSources from '../dataSources';

interface Context {
  ctx: ParameterizedContext;
}

const context = ({ ctx }: Context): ParameterizedContext => ctx;

const server = new ApolloServer({ typeDefs, resolvers, dataSources, context });

export default server;
