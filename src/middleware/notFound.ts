import Boom from 'boom';
import { Context } from '../interfaces';

export default () => (ctx: Context): void => {
  if (ctx.status === 404) {
    ctx.throw(Boom.notFound());
  }
};

// app.use(async(ctx, next) => {
//   try {
//     await next()
//     const status = ctx.status || 404
//     if (status === 404) {
//         ctx.throw(404)
//     }
//   } catch (err) {
//     ctx.status = err.status || 500
//     if (ctx.status === 404) {
//       //Your 404.jade
//       await ctx.render('404')
//     } else {
//       //other_error jade
//       await ctx.render('other_error')
//     }
//   }
// })
