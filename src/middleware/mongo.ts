import mongoose from 'mongoose';
import { ParameterizedContext } from 'koa';

export default () => async (
  ctx: ParameterizedContext,
  next: () => Promise<any>
): Promise<any> => {
  if (process.env.NODE_ENV === 'test') {
    await next();
    return;
  }

  const url = process.env.MONGO_URL;

  await mongoose.connect(url, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });

  await next();
};
