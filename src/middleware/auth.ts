import { Context } from '../interfaces';

const authorize = () => (
  ctx: Context,
  next: () => Promise<any>
): Promise<any> => {
  const loggedIn = !!ctx.session && !!ctx.session.grant;
  ctx.loggedIn = loggedIn;

  if (!loggedIn) return next();

  const {
    oauth_token: accessToken,
    oauth_token_secret: accessSecret,
    screen_name: username,
    user_id: twitterId,
  } = ctx.session.grant.response.raw;

  ctx.username = username;
  ctx.twitterId = twitterId;
  ctx.accessToken = accessToken;
  ctx.accessSecret = accessSecret;

  return next();
};

export { authorize };
