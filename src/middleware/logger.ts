import koaLogger from 'koa-logger';
import { Middleware } from 'koa';

export const logger = {
  log(message?: any, ...optionalParams: any[]): void {
    if (process.env.NODE_ENV === 'test') return;

    console.log(message, ...optionalParams);
  },

  error(message?: any, ...optionalParams: any[]): void {
    if (process.env.NODE_ENV === 'test') return;

    console.error(message, ...optionalParams);
  },
};

type Transporter = (str: string, args: object) => void;

interface TransporterOpts {
  transporter: Transporter;
}

export const requestLogger = (
  opts?: Transporter | TransporterOpts
): Middleware => {
  if (process.env.NODE_ENV === 'test')
    return async (ctx, next): Promise<any> => {
      await next();
    };

  return koaLogger(opts);
};
