import { Context } from 'koa';
import { logger } from '../middleware/logger';
import Boom = require('boom');

const catchErrors = () => async (
  ctx: Context,
  next: () => Promise<void>
): Promise<void> => {
  try {
    await next();
  } catch (err) {
    if (err.isBoom) {
      const error = err as Boom;
      const { statusCode, payload } = error.output;

      ctx.status = statusCode || 500;
      ctx.body = payload;
    }

    ctx.app.emit('error', err, ctx);
  }
};

const handleErrors = () => (err: Boom, ctx: Context): void => {
  /* centralized error handling:
   *   console.log error
   *   write error to log file
   *   save error and request information to database if ctx.request match condition
   *   ...
   */

  logger.error(err);
};

export { catchErrors, handleErrors };
