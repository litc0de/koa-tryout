import notFound from './notFound';
import { Context as BaseContext } from '../interfaces';

interface Context extends BaseContext {
  throw: any;
}

describe('notFound', () => {
  it('catches errors and emits an useful message', async () => {
    const middleware = notFound();
    const throwSpy = jest.fn();
    const ctx = { status: 404, throw: throwSpy } as Context;
    middleware(ctx);

    expect(throwSpy).toHaveBeenCalledWith(new Error('Not Found'));
  });

  it('doesn‘t throw an error if the status is other than 404', async () => {
    const middleware = notFound();
    const throwSpy = jest.fn();
    const ctx = { status: 200, throw: throwSpy } as Context;
    middleware(ctx);

    expect(throwSpy).not.toHaveBeenCalled();
  });
});
