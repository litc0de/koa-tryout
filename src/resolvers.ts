import { Config } from 'apollo-server-koa';
import { conferenceResolvers } from './Conferences';
import { userResolvers } from './User';
import merge from 'deepmerge';

const conf = conferenceResolvers as {};
const usr = userResolvers as {};

const resolvers = merge(conf, usr) as Config['resolvers'];

export default resolvers;
