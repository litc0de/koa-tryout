import { ParameterizedContext } from 'koa';
import { DataSources as BaseDataSources } from 'apollo-server-core/dist/graphqlOptions';
import { ConferenceApi } from './Conferences';
import { UserApi } from './User';

export interface DataSources extends BaseDataSources<Context> {
  Conferences: ConferenceApi;
  User: UserApi;
}

export interface Context extends ParameterizedContext {
  dataSources: DataSources;
  loggedIn: boolean;
  username?: string;
  twitterId?: string;
  accessToken?: string;
  accessSecret?: string;
}
