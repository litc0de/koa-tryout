import mongoose from 'mongoose';

beforeAll(async done => {
  await mongoose.connect(process.env.MONGO_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });

  return done();
});

beforeEach(async done => {
  for (const i in mongoose.connection.collections) {
    await mongoose.connection.collections[i].deleteMany({});
  }

  return done();
});

afterAll(async done => {
  await mongoose.disconnect();

  return done();
});
