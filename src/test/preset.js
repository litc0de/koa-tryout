/* eslint-disable @typescript-eslint/no-var-requires */
const merge = require('deepmerge');
const tsPreset = require('ts-jest/jest-preset');
const mongoDbPreset = require('@shelf/jest-mongodb/jest-preset');
/* eslint-enable @typescript-eslint/no-var-requires */

module.exports = merge(tsPreset, mongoDbPreset);
