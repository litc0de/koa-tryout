import { ApolloServer } from 'apollo-server-koa';
import typeDefs from '../schema';
import resolvers from '../resolvers';
import dataSources from '../dataSources';

export default (context = {}): ApolloServer => {
  const server = new ApolloServer({
    typeDefs,
    resolvers,
    dataSources,
    context,
  });

  return server;
};
