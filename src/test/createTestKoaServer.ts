import app from '../index';
import { Server } from 'http';

export default (): Server => app.listen();
