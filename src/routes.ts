import Router from 'koa-router';
import Boom from 'boom';

const routerOpts: Router.IRouterOptions = { prefix: '/api' };
const router: Router = new Router(routerOpts);

import { userRoutes } from './User';
import { Middleware } from 'koa-compose';
import { ParameterizedContext } from 'koa';

userRoutes(router);

const allowedMethods = (): Middleware<
  ParameterizedContext<any, Router.IRouterParamContext<any, {}>>
> =>
  router.allowedMethods({
    throw: true,
    notImplemented: () => Boom.notImplemented(),
    methodNotAllowed: () => Boom.methodNotAllowed(),
  });

const routes = (): Middleware<
  ParameterizedContext<any, Router.IRouterParamContext<any, {}>>
> => router.routes();

export { routes, allowedMethods };
