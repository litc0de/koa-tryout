import { gql } from 'apollo-server-koa';

export const GET_USER = gql`
  query user {
    user {
      username
      twitterId
      friends
    }
  }
`;
