import BaseApi from '../baseApi';
import request from '../middleware/request';
import { User } from './userInterfaces';

export default class UserApi extends BaseApi {
  async getUser(): Promise<User> {
    const { accessToken, accessSecret, username, twitterId } = this;
    const url = `https://api.twitter.com/1.1/friends/ids.json?screen_name=${username}`;

    const res = await request(url, accessToken, accessSecret);

    if (!res) throw new Error('Request failed');

    const friends: User['friends'] = JSON.parse(res.toString()).ids;
    const user: User = {
      username,
      friends,
      twitterId,
    };

    return user;
  }
}
