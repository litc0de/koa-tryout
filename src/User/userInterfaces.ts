export interface User {
  username: string;
  friends?: string[];
  twitterId: string;
}
