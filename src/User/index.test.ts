import {
  createTestClient,
  ApolloServerTestClient,
} from 'apollo-server-testing';
import nock from 'nock';
import { GET_USER } from './__utils/testQueries';
import { Context } from '../interfaces';
import constructTestServer from '../test/createTestApolloServer';
import userData from '../data/user.mock.json';

describe('User', () => {
  let query: ApolloServerTestClient['query'];

  describe('Queries', () => {
    let twitterId: string;
    let username: string;

    beforeEach(() => {
      twitterId = userData.twitterId;
      username = userData.username;
      const context = (): Context =>
        ({
          loggedIn: true,
          twitterId,
          username,
        } as Context);
      const server = constructTestServer(context);
      const testClient = createTestClient(server);
      query = testClient.query;
    });

    describe('get user', () => {
      it('fetches the current user', async () => {
        nock('https://api.twitter.com')
          .get('/1.1/friends/ids.json?screen_name=lItc0de')
          .reply(200, { ids: userData.friends });

        const res = await query({ query: GET_USER });

        expect(res.data.user).toEqual(userData);
        expect(res).toMatchSnapshot();
      });
    });
  });
});
