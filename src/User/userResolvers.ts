import { Config } from 'apollo-server-koa';
import { Context } from '../interfaces';
import { User } from './userInterfaces';

const resolvers: Config['resolvers'] = {
  Query: {
    async user(_, __, { dataSources }: Context): Promise<User> {
      return dataSources.User.getUser();
    },
  },
  Mutation: {},
};

export default resolvers;
