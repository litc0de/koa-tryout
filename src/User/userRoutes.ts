import Router from 'koa-router';

export default (router: Router): void => {
  router.get('/login', ctx => {
    ctx.body = JSON.stringify(ctx.session.grant.response, null, 2);
    ctx.status = 200;
  });

  router.get('/handle_twitter_callback', ctx => {
    ctx.body = JSON.stringify(ctx.query, null, 2);
  });
};
