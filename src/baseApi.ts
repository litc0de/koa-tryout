import { DataSource, DataSourceConfig } from 'apollo-datasource';
import { User } from './User/userInterfaces';
import { Context } from './interfaces';

export default class BaseApi extends DataSource {
  context: Context;
  attendee: User;
  twitterId?: string;
  username?: string;
  accessToken?: string;
  accessSecret?: string;

  initialize(config: DataSourceConfig<Context>): void {
    const { username, twitterId, accessToken, accessSecret } = config.context;
    this.context = config.context;
    this.username = username;
    this.twitterId = twitterId;
    this.attendee = { twitterId, username };
    this.accessToken = accessToken;
    this.accessSecret = accessSecret;
  }
}
