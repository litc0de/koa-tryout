import { gql } from 'apollo-server-koa';

export default gql`
  type Attendee {
    username: String!
    twitterId: String!
  }

  type User {
    username: String!
    twitterId: String!
    friends: [String]
  }

  type Conference {
    name: String!
    url: String!
    startDate: String!
    endDate: String!
    city: String!
    country: String!
    year: String!
    type: String
    attendees: [Attendee]
    id: String!
    cfpUrl: String
    cfpEndDate: String
    twitter: String
    series: String
  }

  type Query {
    user: User
    conferences(first: Int, offset: Int): [Conference]!
  }

  type Mutation {
    attend(id: String!): Conference!
    deattend(id: String!): Conference!
  }
`;
