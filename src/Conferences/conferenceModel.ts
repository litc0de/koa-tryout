import { Model, Schema, model } from 'mongoose';
import { v1 as uuidv1 } from 'uuid';
import { ConferenceModel } from './conferenceInterfaces';

const conferenceSchema: Schema = new Schema({
  name: String,
  url: String,
  startDate: String,
  endDate: String,
  city: String,
  country: String,
  year: String,
  type: String,
  attendees: Array,
  id: { type: String, default: (): string => uuidv1() },
  cfpUrl: { type: String, default: '' },
  cfpEndDate: { type: String, default: '' },
  twitter: { type: String, default: '' },
  series: { type: String, default: '' },
});

const Conferences: Model<ConferenceModel> = model<ConferenceModel>(
  'Conferences',
  conferenceSchema
);

export default Conferences;
