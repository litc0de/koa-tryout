import {
  createTestClient,
  ApolloServerTestClient,
} from 'apollo-server-testing';
import { ConferenceModel } from './index';
import { User } from '../User/userInterfaces';
import { ATTEND, DEATTEND, GET_CONFERENCES } from './__utils/testQueries';
import constructTestServer from '../test/createTestApolloServer';
import mockConferences from '../data/conferences.mock.json';
import { Context } from '../interfaces';

describe('Conferences', () => {
  let query: ApolloServerTestClient['query'];
  let mutate: ApolloServerTestClient['mutate'];

  beforeEach(async done => {
    await ConferenceModel.create(mockConferences);
    done();
  });

  describe('Queries', () => {
    beforeEach(() => {
      const server = constructTestServer();
      const testClient = createTestClient(server);
      query = testClient.query;
      mutate = testClient.mutate;
    });

    describe('get conferences', () => {
      it('fetches a list of conferences', async () => {
        const res = await query({ query: GET_CONFERENCES });
        expect(res).toMatchSnapshot();
      });
    });
  });

  describe('Mutations', () => {
    let twitterId: string;
    let username: string;
    let attendee: User;
    let id: string;

    beforeEach(() => {
      twitterId = '881575752344498200';
      username = 'lItc0de';
      attendee = { username, twitterId };
      id = '4e1403a0-f7d5-11e9-a84e-fdc6d00929a7';
      const context = (): Context =>
        ({
          loggedIn: true,
          twitterId,
          username,
        } as Context);
      const server = constructTestServer(context);
      const testClient = createTestClient(server);
      query = testClient.query;
      mutate = testClient.mutate;
    });

    describe('attend a conference', () => {
      it('creates a new attend entry for the given conference', async () => {
        const conference = await ConferenceModel.findOne({ id });
        expect(conference.attendees.length).toEqual(0);

        await mutate({
          mutation: ATTEND,
          variables: { id },
        });

        const changedConference = await ConferenceModel.findOne({ id });
        expect(changedConference.attendees[0]).toEqual(attendee);
      });
    });

    describe('deattend a conference', () => {
      it('removes the attend entry for the given conference', async () => {
        await ConferenceModel.updateOne({ id }, { attendees: [attendee] });
        const conference = await ConferenceModel.findOne({ id });
        expect(conference.attendees.length).toEqual(1);

        await mutate({
          mutation: DEATTEND,
          variables: { id },
        });

        const changedConference = await ConferenceModel.findOne({ id });
        expect(changedConference.attendees.length).toEqual(0);
      });
    });
  });
});
