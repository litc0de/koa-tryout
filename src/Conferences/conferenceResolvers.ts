import { Config } from 'apollo-server-koa';
import {
  ConferenceModel,
  GetConferencesArgs,
  AttendArgs,
} from './conferenceInterfaces';
import { Context } from '../interfaces';

const resolvers: Config['resolvers'] = {
  Query: {
    async conferences(
      _,
      { first, offset }: GetConferencesArgs,
      { dataSources }: Context
    ): Promise<ConferenceModel[]> {
      return await dataSources.Conferences.getConferences({ first, offset });
    },
  },

  Mutation: {
    async attend(
      _,
      { id }: AttendArgs,
      { loggedIn, dataSources }: Context
    ): Promise<ConferenceModel> {
      if (!loggedIn) {
        throw new Error('Unauthorized!');
      }
      return await dataSources.Conferences.attendConference({ id });
    },

    async deattend(
      _,
      { id }: AttendArgs,
      { loggedIn, dataSources }: Context
    ): Promise<ConferenceModel> {
      if (!loggedIn) {
        throw new Error('Unauthorized!');
      }
      return await dataSources.Conferences.deattendConference({ id });
    },
  },
};

export default resolvers;
