import {
  GetConferencesArgs,
  ConferenceModel,
  AttendArgs,
} from './conferenceInterfaces';
import Conferences from './conferenceModel';
import { User } from '../User/userInterfaces';
import { Model } from 'mongoose';
import BaseApi from '../baseApi';

export default class ConferenceApi extends BaseApi {
  get model(): Model<ConferenceModel, {}> {
    return Conferences;
  }

  async getConferences({
    first,
    offset,
  }: GetConferencesArgs): Promise<ConferenceModel[]> {
    return await Conferences.find({})
      .skip(offset)
      .limit(first);
  }

  async attendConference({ id }: AttendArgs): Promise<ConferenceModel> {
    const conference = await Conferences.findOne({ id });
    const attendees = [...conference.attendees, this.attendee];

    return await conference.updateOne({ attendees });
  }

  async deattendConference({ id }: AttendArgs): Promise<ConferenceModel> {
    const conference = await Conferences.findOne({ id });
    const attendees = conference.attendees.filter(
      (attendee: User) => attendee.twitterId !== this.twitterId
    );

    return await conference.updateOne({ attendees });
  }
}
