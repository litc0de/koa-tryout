import { User } from '../User/userInterfaces';
import { Document } from 'mongoose';

export interface ConferenceImport {
  name: string;
  url: string;
  startDate: string;
  endDate: string;
  city: string;
  country: string;
  cfpUrl?: string;
  cfpEndDate?: string;
  twitter?: string;
}

export interface Conference extends ConferenceImport {
  year: string;
  type: string;
  attendees: User[];
  series?: string;
}

export interface ConferenceModel extends Document, Conference {}

export interface GetConferencesArgs {
  first: number;
  offset: number;
}

export interface AttendArgs {
  id: string;
}
