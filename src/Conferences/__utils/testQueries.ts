import { gql } from 'apollo-server-koa';

export const GET_CONFERENCES = gql`
  query conferencesList {
    conferences(first: 2) {
      name
      url
      startDate
      endDate
      city
      country
      year
      type
      attendees {
        username
        twitterId
      }
      id
      series
    }
  }
`;

export const ATTEND = gql`
  mutation attend($id: String!) {
    attend(id: $id) {
      attendees {
        username
        twitterId
      }
    }
  }
`;

export const DEATTEND = gql`
  mutation deattend($id: String!) {
    deattend(id: $id) {
      attendees {
        username
        twitterId
      }
    }
  }
`;
