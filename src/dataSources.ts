import { ConferenceApi } from './Conferences';
import { UserApi } from './User';
import { DataSources } from './interfaces';

export default (): DataSources => ({
  Conferences: new ConferenceApi(),
  User: new UserApi(),
});
