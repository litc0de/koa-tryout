import './config';

import Koa from 'koa';

import cors from '@koa/cors';
import session from 'koa-session';
import grant from 'grant-koa';
import mount from 'koa-mount';
import koaqs from 'koa-qs';
import bodyParser from 'koa-bodyparser';

import grantConfig from './config/grant';

import { routes, allowedMethods } from './routes';

import notFound from './middleware/notFound';
import mongo from './middleware/mongo';
import apolloServer from './middleware/apollo';
import { requestLogger } from './middleware/logger';
import { handleErrors, catchErrors } from './middleware/handleErrors';
import { authorize } from './middleware/auth';

const app: Koa = new Koa();

app.keys = ['grant'];
koaqs(app);

app.use(mongo());
app.use(bodyParser());
app.use(cors());

app.use(requestLogger());
app.use(catchErrors());
app.on('error', handleErrors());

app.use(session(app));
app.use(mount(grant(grantConfig)));
app.use(authorize());

app.use(routes());
app.use(allowedMethods());

apolloServer.applyMiddleware({ app });

app.use(notFound());

if (process.env.NODE_ENV !== 'test') {
  app.listen({ port: process.env.PORT }, () => {
    console.log(
      `🚀 Server ready at http://localhost:${process.env.PORT}${apolloServer.graphqlPath}`
    );
  });
}

export default app;
